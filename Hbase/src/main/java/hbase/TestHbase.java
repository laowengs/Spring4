package hbase;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.*;
import org.apache.hadoop.hbase.client.*;

import java.io.IOException;

public class TestHbase {

    public static Configuration configuration;
    static {
        configuration = HBaseConfiguration.create();
//        System.setProperty("HADOOP_HOME","D:\\workSoftware\\apache\\hadoop-2.6.5");
        configuration.set("hbase.zookeeper.property.clientPort", "2181");
        configuration.set("hbase.zookeeper.quorum", "192.168.199.128");
        configuration.set("hbase.master", "192.168.199.128:60000");
    }
    public static void main(String[] args) {
        del("test1");
    }

    private static void del(String tableNameStr){
        System.out.println("start create table ......");
        try {
            Connection connection = ConnectionFactory.createConnection(configuration);
            Admin admin = connection.getAdmin();
            Table table = connection.getTable(TableName.valueOf(tableNameStr));
            TableName tableName = table.getName();
            if(admin.tableExists(tableName)){
                admin.disableTable(tableName);
                admin.deleteTable(tableName);
                System.out.println(tableName + " is exist,detele....");
            }
            HTableDescriptor tableDescriptor = new HTableDescriptor(tableName);
            tableDescriptor.addFamily(new HColumnDescriptor("column123_new"));
            admin.createTable(tableDescriptor);
        } catch (MasterNotRunningException e) {
            e.printStackTrace();
        } catch (ZooKeeperConnectionException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("end create table ......");
    }
}
