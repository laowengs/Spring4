package weng.nio;

import com.alibaba.fastjson.JSON;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;

/**
 * @author wengjp@ffcs.cn
 * @version Revision 1.0.0
 * @版权：福富软件 版权所有 (c) 2018
 * @创建日期：2018-09-28 17:19
 * @功能说明:
 */
public class BufferToText {
    public static final int BSIZE = 1024;

    public static void main(String[] args) throws IOException {
        String chinese = "中文";

        String gbkChinese = new String(chinese.getBytes("GBK"), "ISO-8859-1");
        System.out.println(gbkChinese);

        String unicodeChinese = new String(gbkChinese.getBytes("ISO-8859-1"), "GBK");
        System.out.println(unicodeChinese);

        String utf8Chinese = new String(unicodeChinese.getBytes("UTF-8"), "ISO-8859-1");
        System.out.println(utf8Chinese);

        unicodeChinese = new String(utf8Chinese.getBytes("ISO-8859-1"), "UTF-8");
        System.out.println(unicodeChinese);


    }
}
