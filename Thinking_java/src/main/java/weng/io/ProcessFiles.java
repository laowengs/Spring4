package weng.io;

import java.io.File;
import java.io.IOException;

/**
 * @author wengjp@ffcs.cn
 * @version Revision 1.0.0
 * @版权：福富软件 版权所有 (c) 2018
 * @创建日期：2018-09-26 16:14
 * @功能说明:
 */
public class ProcessFiles {
    public interface Strategy {
        void process(File file);
    }

    private Strategy strategy;
    private String ext;

    public ProcessFiles(Strategy strategy, String ext) {
        this.strategy = strategy;
        this.ext = ext;
    }

    public void start(String[] args) {

        try {
            if (args.length == 0) {
                processDirectoryTree(new File("."));
            }else {
                for (String arg : args){
                    File fileArg  = new File(arg);
                    if(fileArg.isDirectory()){
                        processDirectoryTree(fileArg);
                    }else{
                        if(!arg.endsWith("."+ext)){
                            arg += "." +ext;
                        }
                        strategy.process(new File(arg).getCanonicalFile());
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void processDirectoryTree(File root) throws IOException {
        for (File file : Files.walk(root.getAbsoluteFile(), ".*\\." + ext).files) {
            strategy.process(file.getCanonicalFile());
        }
    }

    public static void main(String[] args) {
        new ProcessFiles(new Strategy(){
            @Override
            public void process(File file) {
                System.out.println(file);
            }

        },"java").start(args);
    }
}
