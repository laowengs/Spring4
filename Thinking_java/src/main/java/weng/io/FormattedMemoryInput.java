package weng.io;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;

/**
 * @author wengjp@ffcs.cn
 * @version Revision 1.0.0
 * @版权：福富软件 版权所有 (c) 2018
 * @创建日期：2018-09-27 16:15
 * @功能说明:格式话内存输入
 */
public class FormattedMemoryInput {
    public static void main(String[] args) throws IOException {
        DataInputStream in = new DataInputStream(new ByteArrayInputStream(BufferedInputFile.read("E:\\Project\\IdeaProject\\SpringForFour\\Thinking_java\\src\\main\\java\\weng\\io\\FormattedMemoryInput.java").getBytes()));
        try {
            while (in.available() != 0) {
                System.out.println((char) in.readByte());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
