package weng.io;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * @author wengjp@ffcs.cn
 * @version Revision 1.0.0
 * @版权：福富软件 版权所有 (c) 2018
 * @创建日期：2018-09-27 15:54
 * @功能说明:从文件读入
 */
public class BufferedInputFile {
    public static String read(String filename) throws IOException {
        BufferedReader in = null;
        try {
            in = new BufferedReader(new FileReader(filename));
        } catch (FileNotFoundException e) {
            System.out.println("文件"+filename+"不存在");
            return "";
        }
        String s;
        StringBuffer stringBuffer = new StringBuffer();

        while((s = in.readLine()) != null ){
            stringBuffer.append(s + "\n");
        }
        try {
            in.close();
        } catch (IOException e) {
            System.out.println("输入流关闭失败");
        }
        return stringBuffer.toString();
    }

    public static void main(String[] args) {
        try {
            System.out.println(read("E:\\Project\\IdeaProject\\SpringForFour\\Thinking_java\\src\\main\\java\\weng\\io\\BufferedInputFile.java"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
