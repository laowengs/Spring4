package weng.io;

import com.alibaba.fastjson.JSON;
import org.junit.Test;
import weng.util.PPrint;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

/**
 * @author wengjp@ffcs.cn
 * @version Revision 1.0.0
 * @版权：福富软件 版权所有 (c) 2018
 * @创建日期：2018-09-26 11:27
 * @功能说明:
 */
public final class Files {


    /**
     * 目录列表器
     * 匿名内部类实现FilenameFilter过滤
     */
    @Test
    public void file() {
        File path = new File(".");
        String[] lists;
        lists = path.list();
        String regex = ".*.xml";
        lists = path.list(new FilenameFilter() {
            Pattern pattern;

            {
                pattern = Pattern.compile(regex);
            }

            @Override
            public boolean accept(File dir, String name) {
                return pattern.matcher(name).matches();
            }
        });
        Arrays.sort(lists, String.CASE_INSENSITIVE_ORDER);
        for (String fileName : lists) {
            System.out.println(fileName);
        }
    }


    /**
     * 目录工具
     */
    public static File[] local(File dir, final String regex) {
        return dir.listFiles(new FilenameFilter() {
            private Pattern pattern = Pattern.compile(regex);

            @Override
            public boolean accept(File dir, String name) {
                return pattern.matcher(new File(name).getName()).matches();
            }
        });
    }

    public static File[] local(String path, String regex) {
        return local(new File(path), regex);
    }

    public static class TreeInfo implements Iterator<File> {
        public List<File> files = new ArrayList<>();
        public List<File> dirs = new ArrayList<>();

        public Iterator<File> iterator() {
            return files.iterator();
        }

        void addAll(TreeInfo other) {
            files.addAll(other.files);
            dirs.addAll(other.dirs);
        }

        @Override
        public String toString() {
            return "TreeInfo{" +
                    "files=" + PPrint.pformat(files) +
                    ", dirs=" + PPrint.pformat(dirs) +
                    '}';
        }


        @Override
        public boolean hasNext() {
            return false;
        }

        @Override
        public File next() {
            return null;
        }


    }

    public static TreeInfo walk(String start, String regex) {
        return recurseDirs(new File(start), regex);
    }

    public static TreeInfo walk(File file, String regex) {
        return recurseDirs(file, regex);
    }

    public static TreeInfo walk(File file) {
        return recurseDirs(file, ".*");
    }

    public static TreeInfo walk(String start) {
        return recurseDirs(new File(start), ".*");
    }

    static TreeInfo recurseDirs(File startDir, String regex) {
        TreeInfo result = new TreeInfo();
        for (File item : startDir.listFiles()) {
            if (item.isDirectory()) {
                result.dirs.add(item);
                result.addAll(recurseDirs(item, regex));
            } else {
                if (item.getName().matches(regex)) {
                    result.files.add(item);
                }
            }
        }
        return result;
    }

    public static void main(String[] args) {
        System.out.println(walk("."));
    }
}

