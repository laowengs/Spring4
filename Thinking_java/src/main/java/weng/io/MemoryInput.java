package weng.io;

import java.io.IOException;
import java.io.StringReader;

/**
 * @author wengjp@ffcs.cn
 * @version Revision 1.0.0
 * @版权：福富软件 版权所有 (c) 2018
 * @创建日期：2018-09-27 16:10
 * @功能说明:从内存输入
 */
public class MemoryInput {
    public static void main(String[] args) throws IOException {
        StringReader in = null;
        try {
            in = new StringReader(BufferedInputFile.read("E:\\Project\\IdeaProject\\SpringForFour\\Thinking_java\\src\\main\\java\\weng\\io\\BufferedInputFile.java"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        int c;
        while((c = in.read()) != -1){
            //read()是以int形式返回下一字节，因此必须类型转换为char
            System.out.println((char)c);
        }
    }

}
