package weng.process;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;

/**
 * @author wengjp@ffcs.cn
 * @version Revision 1.0.0
 * @版权：福富软件 版权所有 (c) 2018
 * @创建日期：2018-10-02 16:34
 * @功能说明:
 */
public class TestPocess {
    public static void main(String[] args) {
        int i = 1;
        while(true){

            System.out.println(i++);
            try {
                Thread.sleep(100L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
