package weng.util;

import java.util.Arrays;
import java.util.Collection;

/**
 * @author wengjp@ffcs.cn
 * @version Revision 1.0.0
 * @版权：福富软件 版权所有 (c) 2018
 * @创建日期：2018-09-26 15:45
 * @功能说明:
 */
public class PPrint {
    public static String pformat(Collection<?> c){
        if(c.size() == 0){
            return  "[]";
        }

        StringBuffer result = new StringBuffer("{");

        for(Object elem : c){
            if(c.size() != 1){
                result.append("\n ");
            }
            result.append(elem);
        }
        if(c.size() != 1){
            result.append("\n ");
        }

        result.append("}");
        return  result.toString();
    }

    public static void pprint(Collection<?> c){
        System.out.println(pformat(c));
    }

    public static void pprint(Object[] c){
        System.out.println(pformat(Arrays.asList(c)));
    }
}
