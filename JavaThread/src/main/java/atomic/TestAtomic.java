package atomic;

import java.util.concurrent.atomic.AtomicLong;

public class TestAtomic extends Thread{

    public static AtomicLong atomicLong = new AtomicLong();
    public static Long longValue;
    @Override
    public void run() {
        for (int i =0;i<999;i++){
            System.out.println(atomicLong.incrementAndGet());
//            try {
//                Thread.sleep(1);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
        }
    }

    public static void main(String[] args) {
        for (int i =0;i<10;i++){
            new TestAtomic().start();
        }
        try {
            Thread.sleep(13);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        new MyThread().start();

        try {
            Thread.sleep(103);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("end ....atomicLong="+atomicLong+"+"+longValue+"="+(atomicLong.get()+longValue));



    }
}
class MyThread extends Thread{
    @Override
    public void run() {
        System.out.println("get atomicLong value:"+TestAtomic.atomicLong.get());
        TestAtomic.longValue = TestAtomic.atomicLong.get();
        System.out.println("set atomicLong value = 0");
        TestAtomic.atomicLong.set(0L);

    }
}
