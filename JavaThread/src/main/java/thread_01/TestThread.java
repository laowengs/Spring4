package thread_01;


import java.util.concurrent.Callable;

/**
 * <out>
 * 运行结束
 * EXTEND THRED
 * </out>
 * <result>代码的运行结果与代码执行顺序或调用顺序是无关的</result>
 */
class Thread_01 extends Thread {
    @Override
    public void run() {
        super.run();
        System.out.println("EXTEND THRED");
    }

    public static void main(String[] args) {
        Thread myThread = new Thread_01();
        myThread.start();
        System.out.println("运行结束");
    }
}

/**
 * <result>线程的执行具有随机性，并且start()方法的顺序不代表线程启动的顺序</result>
 */
class Thread_02 extends Thread {
    @Override
    public void run() {
        try {
            for (int i = 0; i < 10; i++) {
                int time = (int) (Math.random() * 100);
                Thread.sleep(time);
                System.out.println(Thread.currentThread().getName() + "===" + time);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        for (int i = 1; i < 10; i++) {
            Thread thread = new Thread_02();
            thread.setName("thread-" + i);
            thread.start();
        }
    }
}

/**
 * <result>实现RUNNABLE方法创建线程</result>
 */
class Thread_03 implements Runnable {

    @Override
    public void run() {
        System.out.println("IMPLEMENTS RUNNABLE");
    }

    public static void main(String[] args) {
        new Thread(new Thread_03()).start();
        System.out.println("执行完成");
    }
}


/**
 * <result>实现CallAble借口并返回线程执行结果</result>
 */
class Thread_04 implements Callable<String> {

    @Override
    public String call() throws Exception {
        System.out.println("IMPLEMENTS CALLABLES");
        return "CALLABLE RETURN STRING";
    }

    public static void main(String[] args) {
        new Thread(new Thread_03()).start();
        System.out.println("执行成功");
    }
}

/**
 * 线程之间不共享数据
 * <result>每个线程中有单独一个count计数</result>
 */
class Thread_05 extends Thread {
    private int count = 5;

    public Thread_05() {
    }

    public Thread_05(String name) {
        this.setName(name);
    }

    @Override
    public void run() {
        while (count > 0) {
            count--;
            System.out.println(currentThread().getName() + "===" + count);
        }
    }

    public static void noShare() {
        for (int i = 1; i < 4; i++) {
            new Thread_05("" + i).start();
        }
    }

    public static void main(String[] args) {
        noShare();
    }
}

/**
 * 线程之间共享数据
 * ...
 * 16===61
 * 20===61
 * 13===61
 * 17===61
 * 21===61
 * ...
 * <result>线程中公用一个count,不做特殊处理会出现非线程安全问题，多个线程同时取到相同的值</result>
 */
class Thread_06 extends Thread {
    private int count = 100;

    public Thread_06() {
    }

    public Thread_06(String name) {
        this.setName(name);
    }

    @Override
    public void run() {
        count--;
        System.out.println(currentThread().getName() + "===" + count);

    }


    public static void share() {
        Thread_06 myThread = new Thread_06();
        for (int i = 1; i < 40; i++) {
            new Thread(myThread, "" + i).start();
        }
    }

    public static void main(String[] args) {
        share();
    }
}

public class TestThread {

}
