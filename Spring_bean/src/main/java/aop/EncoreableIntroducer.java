package aop;

import org.aspectj.lang.annotation.DeclareParents;

/**
 * @author wengjp@ffcs.cn
 * @version Revision 1.0.0
 * @版权：福富软件 版权所有 (c) 2018
 * @创建日期：2018-10-10 16:50
 * @功能说明:需要引入的切面
 */
public class EncoreableIntroducer {

    @DeclareParents(value = "aop.Performance",defaultImpl = DefaultEncoreable.class)
    public static Encoreable encoreable;
}
