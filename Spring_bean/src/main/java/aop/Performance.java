package aop;

/**
 * @author wengjp@ffcs.cn
 * @version Revision 1.0.0
 * @版权：福富软件 版权所有 (c) 2018
 * @创建日期：2018-10-10 10:43
 * @功能说明:演出
 */
public interface Performance {
    void perform() throws Exception;
    void playTrack(int trackNumber);
}
