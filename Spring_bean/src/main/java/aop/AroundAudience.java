package aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

/**
 * @author wengjp@ffcs.cn
 * @version Revision 1.0.0
 * @版权：福富软件 版权所有 (c) 2018
 * @创建日期：2018-10-10 14:49
 * @功能说明:环绕通知切面
 */
@Aspect
public class AroundAudience {
    @Pointcut("execution(* aop.Performance.perform(..))")
    public void perform(){}

    /**
     * 环绕通知
     * @param joinPoint
     */
    @Around("perform()")
    public void watchPerformance(ProceedingJoinPoint joinPoint){
        try {
            System.out.println("around silencing cell phones");
            System.out.println("around taking seats");
            joinPoint.proceed();
            System.out.println("around clap clap clap");
        } catch (Throwable throwable) {
            System.out.println("around demanding a refud");
        }
    }
}
