package aop;

/**
 * @author wengjp@ffcs.cn
 * @version Revision 1.0.0
 * @版权：福富软件 版权所有 (c) 2018
 * @创建日期：2018-10-10 16:52
 * @功能说明:引入接口默认实现
 */
public class DefaultEncoreable implements Encoreable {
    @Override
    public void performEncore() {
        System.out.println("performEncore");
    }
}
