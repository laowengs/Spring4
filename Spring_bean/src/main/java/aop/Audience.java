package aop;

import org.aspectj.lang.annotation.*;

/**
 * @author wengjp@ffcs.cn
 * @version Revision 1.0.0
 * @版权：福富软件 版权所有 (c) 2018
 * @创建日期：2018-10-10 11:06
 * @功能说明: 观看演出的切面
 */
@Aspect
public class Audience {

    /**
     * 表演之前
     */
    @Before("execution(* aop.Performance.perform(..))")
    public void slienceCellPhones(){
        System.out.println("silence cell phones");
    }

    @Before("execution(* aop.Performance.perform(..))")
    public void takeSeats(){
        System.out.println(" take seat");
    }

    /**
     * 表演之后
     */
    @AfterReturning("execution(* aop.Performance.perform(..))")
    public void applause(){
        System.out.println("clap clap clap");
    }

    /**
     * 表演失败之后
     */
    @AfterThrowing("execution(* aop.Performance.perform(..))")
    public void damandRefund(){
        System.out.println("damanding a refund");
    }

    /**
     * 表演开始和结束之前
     */
    @Around("execution(* aop.Performance.perform(..))")
    public void around(){
        System.out.println("look door");
    }

}
