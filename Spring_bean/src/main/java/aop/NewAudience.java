package aop;

import org.aspectj.lang.annotation.*;

/**
 * @author wengjp@ffcs.cn
 * @version Revision 1.0.0
 * @版权：福富软件 版权所有 (c) 2018
 * @创建日期：2018-10-10 11:06
 * @功能说明: 新的观看演出的切面
 */
@Aspect
public class NewAudience {

    /**
     * 定义命名的切点
     */
    @Pointcut("execution(* aop.Performance.perform(..))")
    public void performance(){}
    /**
     * 表演之前
     */
    @Before("performance()")
    public void slienceCellPhones(){
        System.out.println("silence cell phones");
    }

    @Before("performance()")
    public void takeSeats(){
        System.out.println(" take seat");
    }

    /**
     * 表演之后
     */
    @AfterReturning("performance()")
    public void applause(){
        System.out.println("clap clap clap");
    }

    /**
     * 表演失败之后
     */
    @AfterThrowing("performance()")
    public void damandRefund(){
        System.out.println("damanding a refund");
    }

//    /**
//     * 表演开始和结束之前
//     */
//    @Around("performance()")
//    public void around(){
//        System.out.println("look door");
//    }

}
