package aop;

import org.springframework.stereotype.Component;

/**
 * @author wengjp@ffcs.cn
 * @version Revision 1.0.0
 * @版权：福富软件 版权所有 (c) 2018
 * @创建日期：2018-10-10 14:29
 * @功能说明:
 */
@Component
public class TestPerformance implements Performance {
    @Override
    public void perform() throws Exception {
        System.out.println("this is test proformance");


    }

    @Override
    public void playTrack(int trackNumber) {
        System.out.println("trackNumber:"+trackNumber);
    }
}
