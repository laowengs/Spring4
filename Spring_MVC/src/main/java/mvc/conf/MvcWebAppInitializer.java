package mvc.conf;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

/**
 * @author wengjp@ffcs.cn
 * @version Revision 1.0.0
 * @版权：福富软件 版权所有 (c) 2018
 * @创建日期：2018-10-11 9:44
 * @功能说明:配置DispatcherServlet
 *
 * 扩展AbstractAnnotationConfigDispatcherServletInitializer的任意类都会自动地配置DispatchServlet和Spring应用上下文
 * Spring的应用上下文会位于应用程序的Servlet上下文之中
 *
 * 在Servlet3.0环境中容器会在类路径中查找实现javax.servlet.ServletContainerInitializer接口的类
 * 能发现的话，就会用它来配置Servlet容器
 *
 * Spring提供了这个接口的实现SpringServletContainerInitializer，
 * 这个类会去查找实现了WebApplicationInitializer的类并将配置的任务交给它们
 *
 * Spring3.2引入了便利的WebApplicationInitializer基础实现也就是AbstractAnnotationConfigDispatcherServletInitializer，
 *
 * * 第一个方法getServletMappings()
 *    会将一个或多个路径映射到DisPatcherServlet上
 *
 * > * 为了理解其他两个方法，我们首先要理解DispatcherServlet和一个Servlet监听器(ContextLoaderListener)的关系
 *      当DispatcherServlet启动时，它会创建Spring应用上下文，并加载配置文件或配置类中所声明的bean.
 *
 *      在SpringWeb应用中，通常还会有另外一个应用上下文。另外的应用上下文是由ContextLoaderListener创建的。
 *      我们希望DispatcherServlet加载包含web组件的bean,如控制器，视图解析器以及处理器映射器，
 *      而ContextLoaderListener要加载应用的其他bean。
 *
 *      AbstractAnnotationConfigDispatcherServletInitializer会同时创建DispatcherServlet和ContextLoaderListener。
 *
 *
 *
 *  * 第二个方法 getServletConfigClasses()，
 *      要求DispatcherServlet加载应用上下文时，使用定义在WebConfig配置类中的bean
 *      返回的带有@Configuration注解的类将会用来定义DispatcherServlet应用上下文中的bean
 *    第二个方法 getServletConfigClasses()，
 *      返回的带有@Configuration注解的类将会用来配置ContextLoaderListener创建的应用上下文中的bean
 *
 *
 */
public class MvcWebAppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {
    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class<?>[]{RootConfig.class};
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        //指定配置类
        return new Class<?>[]{WebConfig.class};
    }

    @Override
    protected String[] getServletMappings() {
        //将DispatcherServlet映射到/11
        return new String[]{"/"};
    }
}
