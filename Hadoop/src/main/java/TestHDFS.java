import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;
import org.apache.hadoop.util.Progressable;

import java.io.*;
import java.net.URI;

public class TestHDFS {

    private Configuration conf;
    private URI uri;
    private FileSystem fs;

    public TestHDFS() throws IOException, InterruptedException {
        conf = new Configuration();
        uri = URI.create("hdfs://192.168.199.128:8020");
        fs = FileSystem.get(uri, conf, "hadoop");
    }

    public TestHDFS(Configuration conf, URI uri, FileSystem fs) {
        this.conf = conf;
        this.uri = uri;
        this.fs = fs;
    }


    /**
     * 从hdfs下载文件
     *
     * @param hdfsPath   hdfs文件路径
     * @param resultPath 本地存放路径
     * @throws IOException
     */
    public void down(String hdfsPath, String resultPath) throws IOException {
        FSDataInputStream is = fs.open(new Path(hdfsPath));
        OutputStream out = new FileOutputStream(resultPath);
        IOUtils.copyBytes(is, out, 4096, true);
        System.out.println("下载完成");
    }

    /**
     * 本地文件上传到hdfs
     *
     * @param sourcePath
     * @param destPath
     * @throws IOException
     */
    public void create(String sourcePath, String destPath) throws IOException {
        InputStream in = new BufferedInputStream(new FileInputStream(sourcePath));
        OutputStream outputStream = fs.create(new Path(destPath), new Progressable() {
            @Override
            public void progress() {
                System.out.print(".");
            }
        });
        IOUtils.copyBytes(in, outputStream, 4096, false);
        System.out.println("上载完成");
    }

    /**
     * 创建文件夹
     * @param destDir
     * @throws IOException
     */
    public void mkdirs(String destDir) throws IOException {
        boolean result = fs.mkdirs(new Path(destDir));
        if(result){
            System.out.println("创建成功");
        }else {
            System.out.println("创建失败");
        }
    }

    public void list(String path) throws IOException {
        FileStatus[] fileStatuses = fs.listStatus(new Path(path));
        for (FileStatus fileStatus : fileStatuses){
            System.out.println(fileStatus.getPath());
            if(fileStatus.isDirectory()){
                list(fileStatus.getPath().toString());
            }

        }

    }

    public static void main(String[] args) {
        TestHDFS hdfsUtil = null;
        try {
            hdfsUtil = new TestHDFS();
        } catch (IOException e) {
            System.out.println("生成FileSystem出错");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
//            hdfsUtil.down("/demo/hadoop/core-site.xml", "D:/core-site1.xml");
//            hdfsUtil.create("D:/core-site1.xml", "/demo/hadoop/core-site.xml_bak");
//            hdfsUtil.mkdirs("/demo/test/mkdir/0001");
            hdfsUtil.list("/");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
